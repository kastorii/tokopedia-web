<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Check Official Store</name>
   <tag></tag>
   <elementGuidId>839d14ef-394c-4fa2-9124-d4338a5f0495</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;zeus-root&quot;)/div[@class=&quot;css-8atqhb&quot;]/div[@class=&quot;css-jau1bt&quot;]/div[@class=&quot;css-1c82svt&quot;]/div[@class=&quot;css-gfb3wv&quot;]/div[@class=&quot;css-18nstr8&quot;]/div[1]/div[@class=&quot;css-khi8sg&quot;]/div[1]/div[@class=&quot;css-1ghv7i5&quot;]/div[@class=&quot;css-1m93f3h&quot;]/div[1]/div[@class=&quot;css-1cb34wj&quot;]/div[@class=&quot;css-frokek&quot;]/div[@class=&quot;css-1bh52ki e3y1k2n0&quot;]/div[@class=&quot;css-8wwjx7-unf-checkbox e4ba57s3&quot;]/span[@class=&quot;css-1i85qm8-unf-checkbox__area e3y1k2n1&quot;]</value>
      </entry>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.css-1i85qm8-unf-checkbox__area.e3y1k2n1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[normalize-space()='Official Store']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>631987ac-6753-4235-bffa-ccccb7801ea7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-1i85qm8-unf-checkbox__area e3y1k2n1</value>
      <webElementGuid>1f507062-aa4c-40f6-8f60-0bcefb0f2b1f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;zeus-root&quot;)/div[@class=&quot;css-8atqhb&quot;]/div[@class=&quot;css-jau1bt&quot;]/div[@class=&quot;css-1c82svt&quot;]/div[@class=&quot;css-gfb3wv&quot;]/div[@class=&quot;css-18nstr8&quot;]/div[1]/div[@class=&quot;css-khi8sg&quot;]/div[1]/div[@class=&quot;css-1ghv7i5&quot;]/div[@class=&quot;css-1m93f3h&quot;]/div[1]/div[@class=&quot;css-1cb34wj&quot;]/div[@class=&quot;css-frokek&quot;]/div[@class=&quot;css-1bh52ki e3y1k2n0&quot;]/div[@class=&quot;css-8wwjx7-unf-checkbox e4ba57s3&quot;]/span[@class=&quot;css-1i85qm8-unf-checkbox__area e3y1k2n1&quot;]</value>
      <webElementGuid>dc4d3423-76e1-4778-b4b3-8d4ca01081bb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='zeus-root']/div/div[2]/div/div/div/div/div/div/div[2]/div/div/div/div/div/div/span</value>
      <webElementGuid>9fe61564-899a-4ec9-ab08-3441b9d8823b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jenis toko'])[1]/following::span[1]</value>
      <webElementGuid>68220290-bfad-4008-a0c0-45d35d4a7f42</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Aksesoris Komputer &amp; Laptop'])[1]/following::span[1]</value>
      <webElementGuid>e4da63d2-f6b6-4e57-bf8e-1d0613616567</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Official Store'])[1]/preceding::span[1]</value>
      <webElementGuid>1bd3fe2a-0391-42c9-ac4f-c2dd224a108c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Power Merchant Pro'])[1]/preceding::span[3]</value>
      <webElementGuid>489d6a9c-d760-4bcc-9f21-af002d15af06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/div/span</value>
      <webElementGuid>bbc8b421-8df8-4c10-bc35-f7ef50510854</webElementGuid>
   </webElementXpaths>
</WebElementEntity>

package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class VerifySearchFunctionality {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("user is in Tokopedia Homepage")
	public void user_is_in_Tokopedia_Homepage() {
		WebUI.callTestCase(findTestCase('Function/Open Tokopedia'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("user search on Iphone 14 Pro")
	public void user_search_on_Iphone_14_Pro() {
	WebUI.callTestCase(findTestCase('Function/Input Search Bar'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("user set Official Store")
	public void user_set_Official_Store() {
	WebUI.callTestCase(findTestCase('Function/Click Official Store'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("user set minimum and maximum harga")
	public void user_set_minimum_and_maximum_harga() {
	WebUI.callTestCase(findTestCase('Function/Set Minimum Price'), [:], FailureHandling.STOP_ON_FAILURE)
	WebUI.callTestCase(findTestCase('Function/Set Maximum Price'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("user sort based on Harga Terendah")
	public void user_sort_based_on_Harga_Terendah() {
	WebUI.callTestCase(findTestCase('Function/Urutkan Harga Terendah'), [:], FailureHandling.STOP_ON_FAILURE)
	}
	
	@Then("user obtain all item")
	public void user_obtain_all_item() {
	WebUI.callTestCase(findTestCase('Function/Obtain ALL'), [:], FailureHandling.STOP_ON_FAILURE)
	WebUI.closeBrowser()
	}
}